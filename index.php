<?php
	$page = trim($_SERVER['REDIRECT_URL'], "/");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Business Home \ Mist — Responsive Multi-purpose HTML Template</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="HTML5 Template">
        <meta name="description" content="Mist — Multi-Purpose HTML Template">
        <meta name="author" content="zozothemes.com">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico">
        <!-- Font -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Arimo:300,400,700,400italic,700italic'>
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <!-- Font Awesome Icons -->
        <link href='css/font-awesome.min.css' rel='stylesheet' type='text/css'/>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/hover-dropdown-menu.css" rel="stylesheet">
        <!-- Icomoon Icons -->
        <link href="css/icons.css" rel="stylesheet">
        <!-- Revolution Slider -->
        <link href="css/revolution-slider.css" rel="stylesheet">
        <link href="rs-plugin/css/settings.css" rel="stylesheet">
        <!-- Animations -->
        <link href="css/animate.min.css" rel="stylesheet">

        <!-- Owl Carousel Slider -->
        <link href="css/owl/owl.carousel.css" rel="stylesheet" >
        <link href="css/owl/owl.theme.css" rel="stylesheet" >
        <link href="css/owl/owl.transitions.css" rel="stylesheet" >
        <!-- PrettyPhoto Popup -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <!-- Custom Style -->
        <link href="css/style.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">
        <!-- Color Scheme -->
        <link href="css/color.css" rel="stylesheet">
    </head>
    <body>
        <div id="page">		
			<!-- Page Loader -->
			<div id="pageloader">
				<div class="loader-item fa fa-spin text-color"></div>
			</div>
             <!-- Sticky Navbar -->
			<header id="sticker" class="sticky-navigation">
				<!-- Sticky Menu -->
				<div class="sticky-menu relative">
					<!-- navbar -->
					<div class="navbar navbar-default navbar-bg-light" role="navigation">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="navbar-header">
									<!-- Button For Responsive toggle -->
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="sr-only">Toggle navigation</span> 
									<span class="icon-bar"></span> 
									<span class="icon-bar"></span> 
									<span class="icon-bar"></span></button> 
									<!-- Logo -->
									 
									<a class="navbar-brand" href="index.html">
										<img class="site_logo" alt="Site Logo" src="img/uniform-logo.png" style="width: auto;" />
									</a></div>
									<!-- Navbar Collapse -->
									<div class="navbar-collapse collapse">
										<!-- nav -->
										<ul class="nav navbar-nav">
											<!-- Home  Mega Menu -->
											<li class="mega-menu">
												<a href="/">Home</a>
											</li>
											<!-- Mega Menu Ends -->
											<!-- Pages Mega Menu -->
											<li class="mega-menu">
												<a href="about">About Us</a>
											</li>
											<!-- Pages Menu Ends -->
											<!-- Contact Block -->
											<li>
												<a href="products">Products</a>
												<ul class="dropdown-menu">
													<li>
														<a href="/products#1">Alkyd Resin</a>
													</li>
													<li>
														<a href="/products#2">Rosin Modified Maleic Resin</a>
													</li>
													<li>
														<a href="/products#3">Acrylic Polyol Resin</a>
													</li>
													<li>
														<a href="/products#4">Polyurethane Resin</a>
													</li>
													<li>
														<a href="/products#5">Ketonic Resins</a>
													</li>
													<li>
														<a href="/products#6">Polyamide Resin</a>
													</li>
													<li>
														<a href="/products#7">Rosin Modified Phenolic Resin</a>
													</li>
													<li>
														<a href="/products#8">Polyvinyl Butyral Resin (pvb)</a>
													</li>
													<li>
														<a href="/products#9">Liquid Polyamide Resins</a>
													</li>
													<li>
														<a href="/products#10">Amino Resin</a>
													</li>
												</ul>
											</li>
											<li class="mega-menu">
												<a href="contact">Contact Us</a>
											</li>
										</ul>
										<!-- Right nav -->
									</div>
									<!-- /.navbar-collapse -->
								</div>
								<!-- /.col-md-12 -->
							</div>
							<!-- /.row -->
						</div>
						<!-- /.container -->
					</div>
					<!-- navbar -->
				</div>
				 <!-- Sticky Menu -->
			</header>
			<?php
				switch($page) {
					case "products":
						include("pages/products-header.php");
						include("pages/products.php");
						break;
					case "about-us":
					case "about":
						include("pages/about-header.php");
						include("pages/about.php");
						break;
					case "contact":
						include("pages/contact-header.php");
						include("pages/contact.php");
						break;
					default:
						include("pages/slider.php");
						include("pages/about.php");
						include("pages/contact.php");
				}
			?>

            <!-- request -->
            <footer id="footer">
                <!-- footer-top -->
                <div class="copyright">
                    <div class="container">
                        <div class="row">
                            <!-- Copyrights -->
                            <div class="col-xs-10 col-sm-6 col-md-6"> &copy; 2015 <a href="http://uniformsynthetics.com">Uniform Synthetics</a>
                                <br>
                                <!-- Terms Link -->
                                <a href="#">Site Developed by YNZi</a>
                            </div>
                            <div class="col-xs-2  col-sm-6 col-md-6 text-right page-scroll gray-bg icons-circle i-3x">
                                <!-- Goto Top -->
                                <a href="#page">
                                <i class="glyphicon glyphicon-arrow-up"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer-bottom -->
            </footer>
            <!-- footer -->
        </div>
        <!-- page -->
        <!-- Scripts -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <!-- Menu jQuery plugin -->
        <script type="text/javascript" src="js/hover-dropdown-menu.js"></script>
        <!-- Menu jQuery Bootstrap Addon -->	
        <script type="text/javascript" src="js/jquery.hover-dropdown-menu-addon.js"></script>	
        <!-- Scroll Top Menu -->
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <!-- Sticky Menu -->	
        <script type="text/javascript" src="js/jquery.sticky.js"></script>
        <!-- Bootstrap Validation -->
        <script type="text/javascript" src="js/bootstrapValidator.min.js"></script>	
        <!-- Revolution Slider -->
        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="js/revolution-custom.js"></script>
        <!-- Portfolio Filter -->
        <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
        <!-- Animations -->
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <script type="text/javascript" src="js/effect.js"></script>
        <!-- Owl Carousel Slider -->
        <script type="text/javascript"  src="js/owl.carousel.min.js"></script>
        <!-- Pretty Photo Popup -->
        <script type="text/javascript"  src="js/jquery.prettyPhoto.js"></script>
        <!-- Parallax BG -->
        <script type="text/javascript"  src="js/jquery.parallax-1.1.3.js"></script>
        <!-- Fun Factor / Counter -->
        <script type="text/javascript"  src="js/jquery.countTo.js"></script>
        <!-- PieChart -->
        <script type="text/javascript"  src="js/jquery.easypiechart.min.js"></script>
        <!-- Twitter Feed -->
        <script type="text/javascript" src="js/tweet/carousel.js"></script>
        <script type="text/javascript" src="js/tweet/scripts.js"></script>
        <script type="text/javascript" src="js/tweet/tweetie.min.js"></script>
        <!-- Background Video -->
        <script type="text/javascript" src="js/jquery.mb.YTPlayer.js"></script>
        <!-- Custom Js Code -->
        <script type="text/javascript" src="js/custom.js"></script>
        <!-- Scripts -->
    </body>
</html>