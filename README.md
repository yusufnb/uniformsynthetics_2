# Uniform Synthetics Website #

### Setup ###

* Point a WAMP/MAMP server to the root of this repository
* Make sure mod_redirect is enabled and index.php is the catch all file

### How this site is built ###

* This site is pretty much a static site with PHP includes used for sections and pages of the website
* It uses the mist template - http://zozothemes.com/html/mist/light/index.html

### What remains to be done? ###
* The correct data needs to be populated. Currently there is lot of sample data.
* The products page needs some work