        <section id="team-scroll" class="page-section light-bg">
            <div class="container text-center">
                <div class="section-title">
                    <h2 class="title">About Us</h2>
                </div>
                <div class="row">
                    <div class="owl-carousel navigation-1" data-pagination="false" data-items="4" data-autoplay="true" data-navigation="true">
                        <div class="col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20">
                            <div class="image">
                                <!-- Image -->
                                <img src="img/people-270.jpg" alt="" title="" width="270" height="270" />
                            </div>
                            <div class="description">
                                <!-- Name -->
                                <h4 class="name">People First</h4>
                                <!-- Text -->
                                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.</p>
                            </div>
                        </div>
                        <!-- .employee  -->
                        <div class="col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20">
                            <div class="image">
                                <!-- Image -->
                                <img src="img/nature-270.jpg" alt="" title="" width="270" height="270" />
                            </div>
                            <div class="description">
                                <!-- Name -->
                                <h4 class="name">Sustainable Development</h4>
                                <!-- Text -->
                                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.</p>
                            </div>
                        </div>
                        <!-- .employee -->
                        <div class="col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20">
                            <div class="image">
                                <!-- Image -->
                                <img src="img/quality-270.jpg" alt="" title="" width="270" height="270" />
                            </div>
                            <div class="description">
                                <!-- Name -->
                                <h4 class="name">Pride in Quality</h4>
                                <!-- Text -->
                                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.</p>
                            </div>
                        </div>
                        <!-- .employee -->
                        <div class="col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20">
                            <div class="image">
                                <!-- Image -->
                                <img src="img/simple-270.jpg" alt="" title="" width="270" height="270" />
                            </div>
                            <div class="description">
                                <!-- Name -->
                                <h4 class="name">Keep it Simple</h4>
                                <!-- Text -->
                                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
