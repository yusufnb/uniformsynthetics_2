        <style>
            .product {margin-bottom: 30px;}
        </style>
        <section class="page-section">
            <div class="container shop">
                <div class="row">
                    <div class="col-sm-12 col-md-9 pull-right product-page">
                    
                        <div class="row product">
                            <!-- .product -->
                            <div class="col-md-12 col-sm-12">
                                <div>
                                    <h3>Alkyd Resin</h3>
                                </div>
                                <!--
                                <div class="description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec posuere odio.Phasellus odio lectus, ultrices non pretium ac, mollis id elit.</p>
                                </div>
                                -->
                                <h4>Long Oil Alkyds</h4>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Product </th>
            <th>Oil </th>
            <th>Oil Length </th>
            <th>Solvent</th>
            <th>Solid % </th>
            <th>Acid Value</th>
            <th>Viscosity at 30 C In FCB4</th>
            <th>Uses</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Unikyd<br>L- 60</td>
            <td>Linseed oil</td>
            <td>60</td>
            <td>NA</td>
            <td>99</td>
            <td>15</td>
            <td>50-60<br>50% MTO)</td>
            <td>Letter press, Off set &amp; Screen inks, High gloss paint enamels</td>
        </tr>
        <tr>
            <td>Unikyd<br>L-100</td>
            <td>Linseed oil</td>
            <td>60</td>
            <td>NA</td>
            <td>99</td>
            <td>15</td>
            <td>120-140<br>(50% MTO)</td>
            <td>Primers, Under coats, Architectural finishes, Industrial enamels</td>
            
        </tr>
        <tr>
            <td>Unikyd<br>D-100</td>
            <td>Castor Oil</td>
            <td>60</td>
            <td>NA</td>
            <td>99</td>
            <td>15</td>
            <td>120-140<br>(50% MTO)</td>
            <td>Primers, Under coats, Architectural finishes, Industrial enamels</td>
            
        </tr>
    </tbody>
</table>
                                <div class="product-img">
                                    <img src="/img/products/alkyd-resin1.png"/>
                                </div>
                                <div class="product-img">
                                    <img src="/img/products/alkyd-resin2.png"/>
                                </div>
                            </div>
                        </div>
                        <div class="row product">
                            <!-- .product -->
                            <div class="col-md-12 col-sm-12">
                                <div>
                                    <h3>Rosin Modified Maleic Resin</h3>
                                </div>
                                <div class="product-img">
                                    <img src="http://placehold.it/800x250"/>
                                </div>
                            </div>
                        </div>
                        <div class="row product" id="1">
                            <!-- .product -->
                            <div class="col-md-12 col-sm-12">
                                <div>
                                    <h3>Acrylic Polyol Resin</h3>
                                </div>
                                <div class="product-img">
                                    <img src="http://placehold.it/800x250"/>
                                </div>
                            </div>
                        </div>
                        <div class="row product">
                            <!-- .product -->
                            <div class="col-md-12 col-sm-12">
                                <div>
                                    <h3>Acrylic Polyol Resin</h3>
                                </div>
                                <div class="product-img">
                                    <img src="/img/products/acrylic-polyol-resin.png"/>
                                </div>
                            </div>
                        </div>
                        <div class="row product">
                            <!-- .product -->
                            <div class="col-md-12 col-sm-12">
                                <div>
                                    <h3>Polyurethane Resin</h3>
                                </div>
                                <div class="product-img">
                                    <img src="http://placehold.it/800x250"/>
                                </div>
                            </div>
                        </div>
                        <div class="row product">
                            <!-- .product -->
                            <div class="col-md-12 col-sm-12">
                                <div>
                                    <h3>Ketonic Resins</h3>
                                </div>
                                <div class="product-img">
                                    <img src="/img/products/ketonic-resins.png"/>
                                </div>
                            </div>
                        </div>
                        <div class="row product">
                            <!-- .product -->
                            <div class="col-md-12 col-sm-12">
                                <div>
                                    <h3>Polyamide Resin</h3>
                                </div>
                                <div class="product-img">
                                    <img src="/img/products/polyamide-resin.png"/>
                                </div>
                            </div>
                        </div>
                        <div class="row product">
                            <!-- .product -->
                            <div class="col-md-12 col-sm-12">
                                <div>
                                    <h3>Rosin Modified Phenolic Resin</h3>
                                </div>
                                <div class="product-img">
                                    <img src="/img/products/rosin-modified-maleic-resins.png"/>
                                </div>
                            </div>
                        </div>
                        <div class="row product">
                            <!-- .product -->
                            <div class="col-md-12 col-sm-12">
                                <div>
                                    <h3>Polyvinyl Butyral Resin (pvb)</h3>
                                </div>
                                <div class="product-img">
                                    <img src="/img/products/polyvinyl-butyral-resins.png"/>
                                </div>
                            </div>
                        </div>
                        <div class="row product">
                            <!-- .product -->
                            <div class="col-md-12 col-sm-12">
                                <div>
                                    <h3>Liquid Polyamide Resins</h3>
                                </div>
                                <div class="product-img">
                                    <img src="http://placehold.it/800x250"/>
                                </div>
                            </div>
                        </div>
                        <div class="row product">
                            <!-- .product -->
                            <div class="col-md-12 col-sm-12">
                                <div>
                                    <h3>Amino Resin</h3>
                                </div>
                                <div class="product-img">
                                    <img src="http://placehold.it/800x250"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar col-sm-12 col-md-3">
                        <div class="widget">
                            <div class="widget-title">
                                <h3 class="title">Products</h3>
                            </div>
                            <div id="MainMenu2">
                                <div class="list-group panel">
                                    <div class="collapse in" id="demo3">
                                        <a href="#1" class="list-group-item">Alkyd Resin
                                        <i class="fa fa-caret-down"></i></a>
                                        <a href="#2" class="list-group-item">Rosin Modified Maleic Resin
                                        <i class="fa fa-caret-down"></i></a>
                                        <a href="#3" class="list-group-item">Acrylic Polyol Resin
                                        <i class="fa fa-caret-down"></i></a>
                                        <a href="#4" class="list-group-item">Polyurethane Resin
                                        <i class="fa fa-caret-down"></i></a>
                                        <a href="#5" class="list-group-item">Ketonic Resins
                                        <i class="fa fa-caret-down"></i></a>
                                        <a href="#6" class="list-group-item">Polyamide Resin
                                        <i class="fa fa-caret-down"></i></a>
                                        <a href="#7" class="list-group-item">Rosin Modified Phenolic Resin
                                        <i class="fa fa-caret-down"></i></a>
                                        <a href="#8" class="list-group-item">Polyvinyl Butyral Resin (pvb)
                                        <i class="fa fa-caret-down"></i></a>
                                        <a href="#9" class="list-group-item">Liquid Polyamide Resins
                                        <i class="fa fa-caret-down"></i></a>
                                        <a href="#10" class="list-group-item">Amino Resin
                                        <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!-- category-list -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
