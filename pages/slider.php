        <style>
            .readable { text-shadow: 0px 0px 2px rgba(150, 61, 10, 1); }
        </style>

            <section class="slider" id="home">
                <div id="main-slider" class="carousel">
                    <div data-ride="carousel" class="carousel slide carousel-fade" id="carousel-example-generic1">
                        <!-- Wrapper for slides -->
                        <div role="listbox" class="carousel-inner">
                            <div class="item active">
                                <img width="1920" height="640" title="" alt="" src="img/home-page.jpg">
                                <div class="carousel-caption text-right">
                                    <h1 class="upper animation animated-item-1 white text-right"><span class="text-color">All Synthetic Resins</span><br>  For ink, paint & coating industry</h1>
                                    <p class="readable" style="text-align: left; margin: 0px 0px 10px; padding: 0px; letter-spacing: 0px; font-size: 15px;">Uniform Synthetics has 35 years of experience and expertise of making wide range of innovative Synthetic Resins, developed and customised to meet the requirements of various paints, inks, coatings, adhesives, electro and construction industries.</p>                                    
                                </div>
                            </div>
                        </div>
                        <!-- Controls -->
                        <a data-slide="prev" role="button" href="#carousel-example-generic1" class="left carousel-control">
                        <span aria-hidden="true" class="fa fa-angle-left fa-2x"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a data-slide="next" role="button" href="#carousel-example-generic1" class="right carousel-control">
                        <span aria-hidden="true" class="fa fa-angle-right fa-2x"></span>
                        <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </section>
