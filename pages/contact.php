
            <section id="contact-us" class="page-section">
                <div class="container">
					<div class="row">
                        <div class="col-sm-6 col-md-8">
							<div class="row">
								<div class="col-sm-6 col-md-4">
									 <address>
										<h5 class="title"><i class="icon-map-pin text-color"></i> Address</h5>
										Door no, your Steer address, City,<br>
										 City, state, <br>
										 Country. <br>
										zipcode - xxxxx.
									</address>
								</div>
								<div class="col-sm-6 col-md-4">
									<h5 class="title"><i class="icon-phone10 text-color"></i> Phone</h5>
									<div>Support : +0123 (345) 6789</div>
									<div>Sales : +0123 (345) 6789</div>
									<div>Admin : +0123 (345) 6789</div>
								</div>
								<div class="col-sm-6 col-md-4">
									<h5 class="title"><i class="icon-envelope7 text-color"></i> Email Addresses</h5>
									<div>Support : support@yoursite.com</div>
									<div>Sales : sales@yoursite.com</div>
									<div>Admin : admin@yoursite.com</div>
								</div>
							</div>
							<hr>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						</div>
						<div class="col-md-6 col-md-4">							
							<h3 class="title">Contact Form</h3>
							<p class="form-message" style="display: none;"></p>
							<div class="contact-form">
								<!-- Form Begins -->
								<form role="form" name="contactform" id="contactform" method="post" action="php/contact-form.php">
										<!-- Field 1 -->
										<div class="input-text form-group">
											<input type="text" name="contact_name" class="input-name form-control" placeholder="Full Name" />
										</div>
										<!-- Field 2 -->
										<div class="input-email form-group">
											<input type="email" name="contact_email" class="input-email form-control" placeholder="Email"/>
										</div>
										<!-- Field 3 -->
										<div class="input-email form-group">
											<input type="text" name="contact_phone" class="input-phone form-control" placeholder="Phone"/>
										</div>
										<!-- Field 4 -->
										<div class="textarea-message form-group">
											<textarea name="contact_message" class="textarea-message form-control" placeholder="Message" rows="2" ></textarea>
										</div>
										<!-- Button -->
										<button class="btn btn-default" type="submit">Send Now <i class="icon-paper-plane"></i></button>
									</div>
								</form>
								<!-- Form Ends -->
							</div>
					</div>
                </div>
            </section><!-- page-section -->